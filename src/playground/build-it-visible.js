class VisibilityToggle extends React.Component {
    constructor(props) {
        super(props);
        this.handleToggleVisibility = this.handleToggleVisibility.bind(this);
        this.state = {
            visibility: false
        };
    }

    handleToggleVisibility() {
        this.setState((prevState) => {
            return {
                visibility: !prevState.visibility
            };
        });
    }
    render() {
        return(
            <div>
                <h1>Visibility Toggle</h1>
                <button onClick={this.handleToggleVisibility}>
                    {this.state.visibility ? 'Hide details' : 'Show details'}
                </button>       
                {this.state.visibility && (
                    <div>
                        <p>Hey!.</p>
                    </div>
                )}
            </div>
        );
    }
}

ReactDOM.render(<VisibilityToggle/>, document.getElementById('app'));

/*
const build = {
    title: 'Visibility Toggle'
}

let visibility = false;
let btext = "Show details";

const onShowDetails =() => {
    btext = "Hide details";
    visibility = !visibility;
    render();

};

const appRoot = document.getElementById('app');

const render = () => {
    const templateThree = (
        <div>
        <h1>{build.title}</h1>
        <button onClick={onShowDetails}>{btext}</button>        ***or can directly use the ternary operator***
        {visibility && (
            <div>
                <p>Hey!.</p>
            </div>
        )}
        </div>
    );
    ReactDOM.render(templateThree, appRoot);
};
render();
*/