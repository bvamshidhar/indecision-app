class IndecisionApp extends React.Component {
    constructor(props) {
        super(props);
        this.handleDeleteOptions = this.handleDeleteOptions.bind(this);
        this.handlePick = this.handlePick.bind(this);
        this.handleAddOption = this.handleAddOption.bind(this);
        this.handleDeleteOption = this.handleDeleteOption.bind(this);
        this.state = {
            options: []
        };
    }
    //json(js object notation) is just a string notation of js object or array
    //lifecycle methods -- which are built-in methods which fire at different times in a component's life
    //like when a component gets rendered, updated(props or state) 
    //only accessed(or available) for class based components
    //inside the methods we can access this.state, this.props along with arguments live prevProps, prevState
    componentDidMount() {
        try {
            const json = localStorage.getItem('options');
            const options = JSON.parse(json);           //takes a regular string notation and returns a js object(or array)
        
            if(options) {
                this.setState(() => ({ options }));
            }
        }
        catch(e) {
            //do nothing at all
        }
        
    }

    componentDidUpdate(prevProps, prevState) {
        if(prevState.options.length !== this.state.options.length) {
            const json = JSON.stringify(this.state.options);        //stringify--takes a regular js object and returns a string notation
            localStorage.setItem('options', json);
        }
        
    }
    componentWillUnmount() {
        console.log('will unmount');
    }

    handleDeleteOptions() {
        // this.setState(() => {
        //     return {
        //         options: []
        //     };
        // });
        //can be written as
        this.setState(() => ({ options: [] }));
    }
    handleDeleteOption(optionToRemove) {
        
        this.setState((prevState) => ({
            options: prevState.options.filter((option) => optionToRemove !== option)
        }));        // === returns the value that we actually wanted to remove and removes the remaining items
    }
    handlePick() {
        this.setState(() =>{
            const randomNum = Math.floor(Math.random()* this.state.options.length);
            const option = this.state.options[randomNum];
            alert(option);
        });
        
    }
    handleAddOption(option) {
        if(!option) {
            return 'Enter valid value to add an item';
        }
        else if(this.state.options.indexOf(option) > -1) {      //indexOf return the zero based index i.e, 0 for 1st item, 1 for 2nd item and -1 for a non-existing item
            return 'This option already exists';
        }
        /*
        this.setState((prevState) => {
            return {
                options: prevState.options.concat([option])     //or only .concat(option)
            };

            // prevState.options.push(option);
            // return {
            //     options: prevState.options
            // };
        });
        */
       this.setState((prevState) => ({
           options: prevState.options.concat(option)
        }));
    }
    render() {
        const subtitle = 'Put your life in the hands of a computer.';
        return (
            <div>
                <Header subtitle={subtitle}/>
                <Action
                    hasOptions={this.state.options.length > 0}
                    handlePick={this.handlePick}
                />
                <Options
                    options={this.state.options}
                    handleDeleteOptions={this.handleDeleteOptions}
                    handleDeleteOption={this.handleDeleteOption}
                />
                <AddOption
                    handleAddOption={this.handleAddOption}
                />
            </div>
        );
    }
}



//stateless functional component example --refer to Options
//use class based component when we use somthing advanced like state 
//use stateless functional component for simple components where we just render

//to convert a class based component to a stateless functional component:
//declare a const with the same name and the 1st argument is props,
//then copy paste the return code in.
//the new function which we declare is just same as render()

const Header = (props) => {
    return(
        <div>
            <h1>{props.title}</h1>
            {props.subtitle && <h2>{props.subtitle}</h2>}
        </div>
    );
};

Header.defaultProps = {
    title: 'Indecision'
};

const Action = (props) => {
    return (
        <div>
            <button onClick={props.handlePick}
                disabled={!props.hasOptions}>
                What should I do?
            </button>
        </div>
    );
};


const Options = (props) => {
    return(
        <div>
            <button onClick={props.handleDeleteOptions}>Remove All</button>
            {props.options.length === 0 && <p>Please add an option to get started.</p>}
                {
                    props.options.map((option) => (
                        <Option
                            key={option}
                            optionText={option}
                            handleDeleteOption={props.handleDeleteOption}
                        />
                    ))
                }
        </div>
    );
};

/*
class Options extends React.Component {
    render() {
        return(
            <div>
            <button onClick={this.props.handleDeleteOptions}>Remove All</button>
                {
                    this.props.options.map((option) => <Option key={option} optionText={option}/>)
                }
            </div>
        );
    }
}
*/

const Option = (props) => {
    return(
        <div>
            {props.optionText}
            <button onClick={(e) => {
                props.handleDeleteOption(props.optionText)
            }}>remove</button>
        </div>
    );
};

class AddOption extends React.Component {
    constructor(props) {
        super(props);
        this.handleAddOption = this.handleAddOption.bind(this);
        this.state = {
            error: undefined
        };
    }
    handleAddOption(e) {            //doing something when the it gets submitted
        e.preventDefault();
        
        const option = e.target.elements.option.value.trim();
        const error = this.props.handleAddOption(option);     //passed from parent, which gets things manipulated in terms of that state
        
        // this.setState(() => {
        //     return {
        //         error: error        //or return{error}; is a shorthand
        //     };
        // });
        // shorthands for setState

        this.setState(() => ({ error }));
        if(!error) {
            e.target.elements.option.value = '';
        }
        // or use document.getElementById('addoption').value=""

    }
    render() {
        return(
            <div>
            {this.state.error && <p>{this.state.error}</p>}
            <form onSubmit={this.handleAddOption}>
                <input type="text" id="addoption" name='option'/>
                <button>Add Option</button>
            </form>
            </div>
        );
    }
}


// const User = (props) => {
//     return(
//         <div>
//             <p>Name: {props.name}</p>
//             <p>Age: {props.age}</p>
//         </div>
//     );
// };

ReactDOM.render(<IndecisionApp/>, document.getElementById('app'));